'use strict';

    let randNumber = Math.trunc(Math.random() * 20);
    let score = 20;
    let highscore = 0;

    const message = (message) => {
      document.querySelector(".message").textContent = message;
    };

    const highScoreControl = (score) => {
      document.querySelector(".highscore").textContent = score;
    };

    const lowOrHigh = (val) => {
      scoreAction("fail");
      message(val > randNumber ? "To High!" : "To Low!");
    };

    const checkAnswer = (val) => {
      val === randNumber ? correct() : lowOrHigh(val);
    };

    const play = (val) => {
      val === randNumber ? correct() : lowOrHigh(val);
    };

    const validation = (val) => {
      if (!val) {
        message(isNaN(val) ? "Please a Number!" : "Please val!");
      } else if (score <= 1) {
        gameOver();
      } else {
        play(val);
      }
    };

    const scoreAction = (action) => {
      if (action === "fail") {
        score--;
        console.log(score, "====");
        document.querySelector(".score").textContent = score;
        document.querySelector(".message").textContent = message;
      } else if (action === "reset") {
        document.querySelector(".score").textContent = 20;
      }
    };

    const style = (action) => {
      switch (action) {
        case "gameover":
          document.querySelector("body").style.backgroundColor = "red";
          break;
        case "correct":
          document.querySelector("body").style.backgroundColor = "#60b347";
          break;
        case "reset":
          document.querySelector("body").style.backgroundColor = "#222";
          break;
      }
    };

    const correct = () => {
      message("You are correct!");
      style("correct");
      document.querySelector(".number").textContent = randNumber;
      highScoreControl(score);
    };

    const gameOver = () => {
      message("Game Over");
      style("gameover");
    };

    const reset = () => {
      document.querySelector(".number").textContent = "?";
      randNumber = Math.trunc(Math.random() * 20);
      document.querySelector(".guess").value = "";
      message("Start guessing...");
      scoreAction("reset");
    };

    document.querySelector(".check").addEventListener("click", () => {
      let input = Number(document.querySelector(".guess").value);
      validation(input);
      console.log(randNumber, "+++");
    });

    document.querySelector(".again").addEventListener("click", () => {
      reset();
      style("reset");
    });